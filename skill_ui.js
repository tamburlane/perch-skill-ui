/* 
     Skill UI Toggle switches
     Needed to make checkboxes into toggle switches.  
     
     It just adds a class .selected to the containing element so we
     can select the label with css when the checkbox is checked.
     If the label element was placed after the checkbox we could
     simply select it with CSS '.check:checked label', but that's not 
     the case so we need this bit of JS to help us�
 */

function initCheckboxes() {
  // Select all inputs
  var inputs = document.getElementsByTagName("input");

  // Find all checkboxes
  for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == "checkbox") {
      var cb = inputs[i];

      //Find the parent element where the checkbox live
      var parent = findAncestor(cb, ".checkbox-single");
      // Add a class of selected to the parent
      // That's what we will use in the CSS to target the label
      if (cb.checked == true) {
        parent.classList.add("selected");
      }
      // Toggle the class on change
      cb.addEventListener(
        "change",
        function() {
          var parent = findAncestor(this, ".checkbox-single");
          parent.classList.toggle("selected");
        },
        false
      );
    }
  }
}

/* Helper function */

function findAncestor(el, sel) {
  while (
    (el = el.parentElement) &&
    !(el.matches || el.matchesSelector).call(el, sel)
  );
  return el;
}

/* Run it */

// in case the document is already rendered
if (document.readyState != "loading") initCheckboxes();
// modern browsers
else if (document.addEventListener)
  document.addEventListener("DOMContentLoaded", initCheckboxes);
// IE <= 8
else
  document.attachEvent("onreadystatechange", function() {
    if (document.readyState == "complete") initCheckboxes();
  });
