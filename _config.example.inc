<!--
    Skill UI 
    Custom login screen CSS and JS
    
    Note: Admin CSS and JS are loaded with Pipit UI Loader.
    We can't use that for the login screen so load login resources here.
-->
<link rel="stylesheet" href="/admin/addons/plugins/ui/skill/skill_ui_login.css" title="Login CSS" type="text/css" media="screen" charset="utf-8">
<script src="/admin/addons/plugins/ui/skill/skill_ui_login.js" charset="utf-8"></script>