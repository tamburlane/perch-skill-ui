<?php
// Custom admin UI 
// Custom redactor styles 
// Favicon
return [
    'css' => [
        '/admin/addons/plugins/ui/skill/skill_ui.css'
    ],

    'js' => [
        '/admin/addons/plugins/ui/skill/skill_ui.js'
    ],

    'favicon' => '/img/favicons/favicon.ico',
];