# Skill UI for Perch CMS

A custom UI style that makes your CMS look like the cool kids. It fixes some annoyances with the original Perch UI as well as turning all your single checkboxes to toggle switches, too. 

Find [installation instructions and screenshots](https://bitbucket.org/tamburlane/perch-skill-ui/wiki/) in the Wiki.  

