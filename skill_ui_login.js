function initLogin() {
  var userInput = document.getElementsByName("username")[0];
  var passInput = document.getElementsByName("password")[0];
  if (userInput) {
    userInput.placeholder = "Username";
  }
  if (passInput) {
    passInput.placeholder = "Password";
  }
}

// in case the document is already rendered
if (document.readyState != "loading") initLogin();
// modern browsers
else if (document.addEventListener)
  document.addEventListener("DOMContentLoaded", initLogin);
// IE <= 8
else
  document.attachEvent("onreadystatechange", function() {
    if (document.readyState == "complete") initLogin();
  });
